import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Icon } from "native-base";

class CustomThumb extends Component {
  render() {
    return (
      <View
        style={[
          styles.cardlayout,
          { backgroundColor: this.props.backgroundColor }
        ]}
      >
        <Image />
        <Icon
          name={this.props.iconName}
          type={this.props.iconType}
          style={styles.icon}
        />
        <Text style={styles.text}>{this.props.title} </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardlayout: {
    height: 100,
    width: 200,
    borderRadius: 5,
    marginLeft: 20,
    marginTop: 10
  },
  text: {
    textAlign: "center",
    color: "white",
    fontSize: 16,
    marginTop: 10
  },
  icon: {
    color: "white",
    fontSize: 40,
    textAlign: "center",
    marginTop: 10
  }
});

export default CustomThumb;
