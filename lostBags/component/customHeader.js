import React, { Component } from "react";
import TopHeader from "./topHeader";
import Home from "./home";
import Info from "../pages/info";
import {
  ScrollView,
  StyleSheet,
  View,
  Animated,
  Text,
  Platform,
  Image,
  ImageBackground
} from "react-native";
import { CardItem, Tab, Tabs, TabHeading, Icon } from "native-base";

const Header_Maximum_Height = 250;

const Header_Minimum_Height = 50;

export default class CustomHeader extends Component {
  constructor() {
    super();

    this.AnimatedHeaderValue = new Animated.Value(0);
  }

  render() {
    const AnimateHeaderBackgroundColor = this.AnimatedHeaderValue.interpolate({
      inputRange: [0, Header_Maximum_Height - Header_Minimum_Height],

      outputRange: ["#779787", "#1F3162"],

      extrapolate: "clamp"
    });

    const AnimateHeaderHeight = this.AnimatedHeaderValue.interpolate({
      inputRange: [0, Header_Maximum_Height - Header_Minimum_Height],

      outputRange: [Header_Maximum_Height, Header_Minimum_Height],

      extrapolate: "clamp"
    });

    return (
      <View style={styles.MainContainer}>
        <ScrollView
          scrollEventThrottle={16}
          contentContainerStyle={{ paddingTop: Header_Maximum_Height }}
          onScroll={Animated.event([
            {
              nativeEvent: { contentOffset: { y: this.AnimatedHeaderValue } }
            }
          ])}
        >
          {/*  My Component  */}
          <Home />
          <View style={{ marginBottom: 25 }}>
            <CardItem cardBody>
              <Image
                source={require("../images/ad.jpg")}
                style={{
                  height: 170,
                  width: 300,
                  marginTop: 20,
                  marginLeft: 55,
                  resizeMode: "cover"
                }}
              />
            </CardItem>
          </View>
        </ScrollView>

        <Animated.View
          style={[
            styles.HeaderStyle,
            {
              height: AnimateHeaderHeight,
              backgroundColor: AnimateHeaderBackgroundColor
            }
          ]}
        >
          <ImageBackground
            source={require("../images/header.jpg")}
            style={{ width: "100%", height: "100%", resizeMode: "cover" }}
          >
            <TopHeader />
            {/* <Text style={styles.HeaderInsideTextStyle}>Home</Text> */}
          </ImageBackground>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: Platform.OS == "ios" ? 20 : 0
  },

  HeaderStyle: {
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    left: 0,
    right: 0,
    top: Platform.OS == "ios" ? 20 : 0
  },

  HeaderInsideTextStyle: {
    color: "#fff",
    fontSize: 22,
    textAlign: "center"
  },

  TextViewStyle: {
    textAlign: "center",
    color: "#000",
    fontSize: 18,
    margin: 5,
    padding: 7,
    backgroundColor: "#ECEFF1"
  }
});
