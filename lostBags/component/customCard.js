import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Icon } from "native-base";

class CustomCard extends Component {
  render() {
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: this.props.backgroundColor }
        ]}
      >
        <Icon
          name={this.props.iconName}
          type="MaterialCommunityIcons"
          style={styles.icon}
        />
        <Text style={styles.text}>{this.props.title} </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // height: 110,
    // width: 187,
    flex: 1,
    borderRadius: 5,
    paddingTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginLeft: 8,
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 15,
    flex: 1,
    paddingTop: 20
  },
  icon: {
    flex: 1,
    color: "white",
    fontSize: 40
  }
});

export default CustomCard;
