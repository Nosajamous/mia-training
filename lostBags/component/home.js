import React, { Component } from "react";
import { StyleSheet, View, ScrollView, Image } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container, Content, Icon, Text } from "native-base";
import CustomCard from "./customCard";
import CustomThumb from "./customThumb";
import CustomWeather from "./customWeather";

export default class Home extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Grid>
            <Row
              style={{
                height: 62,
                flexDirection: "column"
              }}
            >
              <Text
                style={{
                  flex: 2.4,
                  fontSize: 30,
                  textAlign: "center"
                }}
              >
                Good Morning
              </Text>
              <Text
                style={{
                  flex: 1,
                  fontSize: 13,
                  color: "#C1C1C1",
                  textAlign: "center"
                }}
              >
                Select a flight and we'll keep you posted.
              </Text>
            </Row>

            {/* card Item section */}
            <Row>
              <Col style={{ height: 135 }}>
                <CustomCard
                  title="Departures"
                  iconName="airplane-takeoff"
                  backgroundColor="#535C8D"
                />
              </Col>
              <Col style={{ height: 135 }}>
                <CustomCard
                  title="Arrivals"
                  iconName="airplane-landing"
                  backgroundColor="#4682B4"
                />
              </Col>
            </Row>
            {/* card Item section ended */}
            <Row
              style={{
                flex: 1,
                height: 85,
                borderStyle: "solid",
                borderTopWidth: 1,
                borderTopColor: "#ADADAE",
                marginTop: 10
              }}
            >
              <Col style={{ flex: 1 }}>
                <Icon
                  name="human-greeting"
                  type="MaterialCommunityIcons"
                  style={{
                    fontSize: 50,
                    color: "#4090EA",
                    textAlign: "center",
                    paddingTop: 15
                  }}
                />
              </Col>
              <Col style={{ flex: 4 }}>
                <Row style={{ flex: 1, paddingTop: 15 }}>
                  <Text>Mobile Passport Control</Text>
                </Row>
                <Row style={{ flex: 2.5 }}>
                  <Text style={{ fontSize: 13 }}>
                    Submit your CBP form for entry into the US.
                    <Text
                      style={{
                        fontSize: 13,
                        color: "#0E59C8"
                      }}
                    >
                      Tap To Start
                    </Text>
                  </Text>
                </Row>
              </Col>
            </Row>
            {/* Weather Implementation */}
            <Row
              style={{
                flex: 1,
                flexDirection: "column",
                height: 165,
                borderStyle: "solid",
                borderTopWidth: 1,
                borderTopColor: "#ADADAE"
              }}
            >
              <Row
                style={{
                  flex: 1
                }}
              >
                <Text
                  style={{
                    padding: 5,
                    fontSize: 16,
                    marginLeft: 16
                  }}
                >
                  Weather Forcast in Miami
                </Text>
              </Row>
              <Row style={{ flex: 3 }}>
                <ScrollView horizontal>
                  <CustomWeather dayName="Sat" iconName="cloud-meatball" />
                  <CustomWeather dayName="Sun" iconName="cloud-sun-rain" />
                  <CustomWeather dayName="Mon" iconName="cloud-showers-heavy" />
                  <CustomWeather dayName="Tue" iconName="cloud-sun" />
                  <CustomWeather dayName="Wed" iconName="cloud-moon-rain" />
                  <CustomWeather dayName="Thu" iconName="cloud-meatball" />
                  <CustomWeather dayName="Fri" iconName="cloud" />
                </ScrollView>
              </Row>
            </Row>
            {/* Weather Implementation ended */}

            {/* Explore Mia Started ---------- */}
            <Row
              style={{
                flex: 1,
                flexDirection: "column",
                height: 160,
                borderStyle: "solid",
                borderTopWidth: 1,
                borderTopColor: "#ADADAE"
              }}
            >
              <Row
                style={{
                  flex: 1
                }}
              >
                <Text
                  style={{
                    padding: 5,
                    fontSize: 16,
                    marginLeft: 16
                  }}
                >
                  Explore MIA
                </Text>
              </Row>
              <Row style={{ flex: 3 }}>
                <ScrollView horizontal>
                  <CustomThumb
                    title="Dining"
                    iconType="MaterialIcons"
                    iconName="restaurant"
                    backgroundColor="#1E3283"
                  />
                  <CustomThumb
                    title="Shopping"
                    iconType="Foundation"
                    iconName="shopping-cart"
                    backgroundColor="#4682B4"
                  />
                  <CustomThumb
                    title="Airlines"
                    iconType="MaterialIcons"
                    iconName="airplanemode-active"
                    backgroundColor="#97D3F0"
                  />
                  <CustomThumb
                    title="Parking"
                    iconType="FontAwesome5"
                    iconName="parking"
                    backgroundColor="#0E171C"
                  />
                  <CustomThumb
                    title="Rental Cars"
                    iconType="Ionicons"
                    iconName="md-car"
                    backgroundColor="#31438A"
                  />
                  <CustomThumb
                    title="Transportation"
                    iconType="FontAwesome"
                    iconName="bus"
                    backgroundColor="#5D6EB2"
                  />
                  <CustomThumb
                    title="Art Exhibits"
                    iconType="MaterialCommunityIcons"
                    iconName="cookie"
                    backgroundColor="#92C9E9"
                  />
                  <CustomThumb
                    title="ATM & Banks"
                    iconType="Foundation"
                    iconName="dollar"
                    backgroundColor="#373E5B"
                  />
                  <CustomThumb
                    title="VIP Clubs & Lounges"
                    iconType="Foundation"
                    iconName="torso-business"
                    backgroundColor="#2A345E"
                  />
                </ScrollView>
              </Row>
            </Row>
            {/* Explore Mia Ended ---------- */}
            {/* Connect With Us Started ---------- */}
            <Row
              style={{
                flex: 1,
                flexDirection: "column",
                height: 160,
                borderStyle: "solid",
                borderTopWidth: 1,
                borderTopColor: "#ADADAE",
                marginTop: 5
              }}
            >
              <Row
                style={{
                  flex: 1
                }}
              >
                <Text
                  style={{
                    padding: 5,
                    fontSize: 16,
                    marginLeft: 16
                  }}
                >
                  Connect With US
                </Text>
              </Row>
              <Row style={{ flex: 3 }}>
                <ScrollView horizontal>
                  <CustomThumb
                    title="iflyMIA"
                    iconType="AntDesign"
                    iconName="facebook-square"
                    backgroundColor="#3355B3"
                  />
                  <CustomThumb
                    title="iflyMIA"
                    iconType="FontAwesome"
                    iconName="twitter-square"
                    backgroundColor="#39A8F2"
                  />
                  <CustomThumb
                    title="iflyMIA"
                    iconType="FontAwesome"
                    iconName="instagram"
                    backgroundColor="#27565B"
                  />
                  <CustomThumb
                    title="www.miami-airport.com"
                    iconType="FontAwesome5"
                    iconName="plane"
                    backgroundColor="#5271B0"
                  />
                </ScrollView>
              </Row>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderStyle: "solid",
                  borderBottomColor: "#ADADAE",
                  marginTop: 8
                }}
              />
            </Row>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({});
