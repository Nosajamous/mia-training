import React, { Component } from "react";
import {
  createDrawerNavigator,
  createAppContainer,
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";
import CustomHeader from "./customHeader";
import SettingScreen from "./settingScreen";
import AirlineDetails from "./airlineDetails";
import Airlines from "../pages/airlines";
import { Icon } from "native-base";

export default class MainApp extends Component {
  componentDidMount() {
    console.disableYellowBox = true;
  }
  render() {
    return <App />;
  }
}

const stackNav = createStackNavigator({
  Info: {
    screen: Airlines,
    navigationOptions: () => ({
      title: `AirLines`,
      headerBackTitle: true
    })
  },
  AirlineDetails: {
    screen: AirlineDetails,
    navigationOptions: () => ({
      title: `AirlineDetails`,
      headerBackTitle: true
    })
  }
});

const TabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: CustomHeader,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            name="home"
            size={23}
            color="#C1C1C1"
            style={{ color: "grey" }}
          />
        )
      }
    },
    Flight: {
      screen: SettingScreen,
      navigationOptions: {
        tabBarLabel: "Flight",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            name="airplanemode-active"
            type="MaterialIcons"
            size={23}
            color="#C1C1C1"
            style={{ color: "grey" }}
          />
        )
      }
    },
    Explore: {
      screen: SettingScreen,
      navigationOptions: {
        tabBarLabel: "Explore",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            name="explore"
            type="MaterialIcons"
            size={23}
            color="#C1C1C1"
            style={{ color: "grey" }}
          />
        )
      }
    },
    Map: {
      screen: SettingScreen,
      navigationOptions: {
        tabBarLabel: "Map",
        tabBarIcon: ({ tintColor }) => (
          <Icon
            name="google-maps"
            type="MaterialCommunityIcons"
            size={23}
            color="#C1C1C1"
            style={{ color: "grey" }}
          />
        )
      }
    },
    Menu: {
      screen: stackNav,
      navigationOptions: {
        tabBarLabel: "Menu",
        tabBarIcon: ({ tintColor, focused }) => (
          <Icon
            name="menu"
            type="Entypo"
            size={23}
            color="#C1C1C1"
            style={{ color: "grey" }}
          />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: "black"
    }
  }
);

// const navigator = createDrawerNavigator(
//   {
//     Home: {
//       screen: CustomHeader
//     },
//     Settings: {
//       screen: SettingScreen
//     },
//     Airlines: {
//       screen: stackNav
//     }
//   },
//   {
//     drawerPosition: "left",
//     initialRouteName: "Home",
//     drawerBackgroundColor: "white",
//     drawerWidth: 280
//   }
// );

const App = createAppContainer(TabNavigator);
