import React, { Component } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Icon, Right, Left } from "native-base";
import { Row } from "react-native-easy-grid";

class TopHeader extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Left>
          <Icon
            style={styles.userIcon}
            name="user-circle-o"
            type="FontAwesome"
          />
        </Left>

        <Text style={styles.headerTitle}>MIA</Text>
        <Row style={styles.rightIcon}>
          <Icon
            style={{
              color: "#fff",
              position: "absolute",
              right: 0
            }}
            name="md-search"
            type="Ionicons"
          />
          <Icon
            style={{ color: "#fff", position: "absolute", right: 35 }}
            name="message-text"
            type="MaterialCommunityIcons"
          />
        </Row>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    height: 10,
    paddingTop: 7
  },
  userIcon: {
    flex: 1,
    color: "#fff"
  },
  headerTitle: {
    flex: 1,
    textAlign: "center",
    color: "#fff",
    fontSize: 22,
    fontWeight: "bold"
  },
  rightIcon: {
    flex: 1,
    flexDirection: "row",
    color: "#fff"
  }
});

export default TopHeader;
