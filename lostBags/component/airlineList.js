import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Row } from "react-native-easy-grid";
import { Icon } from "native-base";

export default class AirlineList extends Component {
  render() {
    return (
      <Row style={styles.rowStyle}>
        <Row
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              flex: 7,
              color: "#164093"
            }}
          >
            {this.props.miniLocation}
          </Text>
          <Icon
            style={{
              flex: 1,
              textAlign: "center",
              color: "#164093"
            }}
            name={this.props.iconName}
            type={this.props.iconType}
          />
        </Row>
        <Row
          style={{
            flex: 2,
            flexDirection: "column",
            paddingTop: 5
          }}
        >
          <Text style={{ fontSize: 16, color: "black" }}>
            {this.props.location} {this.props.floor}
          </Text>
        </Row>
      </Row>
    );
  }
}

const styles = StyleSheet.create({
  rowStyle: {
    flexDirection: "column",
    borderBottomColor: "#E8E9E9",
    borderBottomWidth: 1,
    height: 80,
    paddingTop: 10,
    paddingLeft: 10
  }
});
