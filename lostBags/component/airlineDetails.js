import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Row } from "react-native-easy-grid";
import AirlineList from "./airlineList";
import { ScrollView } from "react-native-gesture-handler";

export default class AirlineDetails extends Component {
  url =
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMHBhMQExAVFhIVEhUTExMYEBUXFhUSFxIWFxoVGBYYHSggGBomHRUYIjEhJSkrLi4vFyszODMsNygtLisBCgoKDg0OGxAQGi0lHyUuLS8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABgcDBAUCAf/EAD4QAAIBAgIGBwUGBAcBAAAAAAABAgMEBREGEiExQWETIlFScYGRFDKhscEVM0JjcvAjYpKigpOy0dLh8Qf/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAwQFAgEG/8QAKxEBAAICAgIBAwMDBQAAAAAAAAECAxEEEiExURMyQQUiYUJxkRQjYoGx/9oADAMBAAIRAxEAPwC8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOZjOMwwqnt2zfuwT2vm+xFfPya4o8+/hHfJFUQvNJLi5lsnqLsisvjvMq/Ny2n3r+ytOa0vuH6SV7WsnKbnDPrRlteXJ78xi5uSs+Z3D2uW0T5TO+xSnZWiqSlsks4pb5bM9iNfJnpSvaZWbXisblEb/SqtcSahlTjwy2y82/oZeTn5LT+3xCtbNafTQhjVxCefTzz5yzXo9hBHJyx/VLj6l/lL9GsaeKU5Rnkqkcm8t0o9uXA1eJyZyxMW9wtYsnb27hcSgAAAAAAAAAAAAAAAAAAAAAADSxe/jhtjKo+GyK7ZPciHNljHSbS5vbrG1b3NxK6ruc3nKTzb/fA+fve157SoTO52xHLwAzXFzK5knJ56sVGPYopZJI6veb+3U2mfbCcuQCQ6ERbxaT4Km8/6ol/9Pj/AHJ/snwfcnJsrYAAAAAAAAAAAAAAAAAAAAAAAg+ml702IKkt1Nbf1SWfyy9TG/UMna8V+FTPbc6R0oIAAAAAA9THQS1atJ12tk3qw/THPb5tv0Nrg4ele0/lY40bibJSXlkAAAAAAAAAAAAAAAAAAAAAAAVhi9TpcUqy/Ml6J5L5Hzme28lp/lQvO7S1CJwAAAADLhdjLGsQVGGyK21Z92HZ4vd/4y7xuP3tuUcROW3SPX5Wjb0Y29CMIrKMUoxXYkskjaiNRpq1iIjUMh69AAAAAAAAAAAAAAAAAABrXd/Ss/vKkY8m9vpvI75aU+6dPJtEe2nHSK2lLLpl5xkl6tEUczDP9Tj6tPl0aNaNenrRkpJ8U016osVtFo3DuJ36ZD16rHF6fRYrVj+ZL0bzXzPnM9euS0fyoXjVpaZE4AAADzaW9TFrtUaKzb96XCMe1vgvmW8HHm0od2y260WXgmEwweyVOG/fKWW2Uu1/7G1jxxSNQ08OKMVesOidpQAAAAAAAAAAAAAAAAAARrSfHnZvoab/AImXWl3U+C5mfy+V0/ZX2gy5NeIQucnObbbbe9t5t+ZkTMzO5Vfb4ePG1h2IVMOr60JZdseEl2NEuLLbHO6u63mvpYeFYjHE7RVI+Eo8Yy7Ddw5oy07Qu0tFo2iWmlp0OJqpwqR/ujsfwyMvn49ZO3yrZq6naPlFAAeak1Tjm2exWbeIeWtFfMs2E4RWx+rlFatJPrVGtngu8+RfwcWZRUpfPPj0sfB8Kp4Ra9HTXOUn70n2tmrSkUjUNTFirjjUN87SAAAAAAAAAAAAAAAAAAAxXNboLeU3ujFy9Fmc3t1rM/DyZ1G1W1qrr1nOTzlJtvxZ83a02mZlnzO5eDl4AAOto1iX2fiKzfUnlGXLsl5P4MtcTN9PJ59SlxX6ymOkGHfaWHOK99daH6lw81sNXk4fq49R7/Czkr2hXLWq8nvWxrmYExMeFJ8pxlcVujpwc592Kzy5t8CbFhteUc2n1WNykuD6Fa0lUupZvhSi9i5Slx8F6s1cPEiv3JsfDmZ7ZP8ACXxULSgl1YQislujFL5It+KwvREVjTmXWktvb/j132QWfx3fErX5uKv53/ZHOakflzaumaT6tFvxml8kyvb9Sr+IcTyI+GJaaSz+4X+Y/wDicx+pf8Xn+o/hvWml1Gs8pxlDn70fVbfgTU/UMc+/Duues+3eo1o16alGSknuaeaLtbRaNxKWJ36ZDp6AAAAAAAAAAAAAAw3dH2i1nDvRlH1TRxevasw8mNxpWF1bStK7hOLUlvX1Xaj5y9JpPWVC0TE6liOXIAAAWPo7ee3YTCT95LUl4x2Z+ayfmfQcXJ9TFEr+O3au2C+0bo3t66stZZ+9FPJSfa8tq8jm/Ex2v2lxbDW07bMp2+B2uWUKceCS2yfgtsmSWvjxV8+HWqY4+EdxHS6U81RjqrvS2y8luXxM/L+oWnxSENs/wj1zdTup5znKT5vP0XAoXyWvO7TtDNpn2wnDkAAAN/CMVnhdxrRecW+tDhJfR8yfBntituPSSl5rKx6FVV6MZxecZJNPk1mb9bRaImF2J3G2Q6egAAAAAAAAAAAARzSbHnYvoqb/AIjXWl3E/qUOXyvp/tr7/wDEGXL18QhVSo6s3KTbb3tvNvzMebTM7lVmdvJ48AAHu3oyurmNKEdacty5cZN8Eu0lxYrZLag8zPWPaycHw9YZYqmnm98pd6T3v99hvYsUY69YX8desacbHNK42td0aOUqiz1pfhhy/ml++RX5PK+nH7faC/JiLda+ZRG4uJXNZznJyk97b/eSMa97Xndp3KGZmZ3LEcuQAAAAAAFi6L5/YNLPsl6a8svhkb/E39Gu17F9kOqWUgAAAAAAAAAAAMdxVVChKb3Ri5PwSzObW6xMvJ+VWV6zuK0pyfWk234s+bvabW7Sz5nc7eDl4AAPtrSnf3SpUY603vf4Yrtk+CLGHj2yS47TaetPKwtH8Dhg9vv1qsvfqPe+S7I8jbxYa441C/hwxjj5n5cDTPSZ0c7ejLrbqk1w/ljz5lbPn3utf+1bl8nrHSvtFLK0nTt1VccoSbjF97LLPLlt3mfyImKxKpx6T90s5VWAAAAAAAGW0t5XdzGnFdaTyX1fgltO8WOclorD2I3OoWfaUFa20aa3RioryWR9HWsVrEQ0IjUaZjp6AAAAAAAAAAADDd0faLWcO9CUfVNHN47VmHk+YVdWpSoVnCSylF5Ncz5q1ZrPWfbPmNTp4PHjDO5inkus3uS25skritKO2WsTqPLtYXotcYm1KrnRpdn42vDh5+hoYeF+Zd0wZMv3eITnDMMpYVb6lKGS4ve5Ptb4s0a0rWPC/jxVxxqrnaWYz9mWWrF/xJJ5fyx4y8eC/wCity8/SOse5cZ8vSqC6NYNLHsS62fRxetUl8op9r+RBgw9p18MvBhnNfc+ks01oKjY0VFJRjJxSW5LVWS/tH6hX9tdNLPGqxpETKVgAAAAfHNLivU96y87R8vdpTlfV+jpQc5cty5yluSJsfHvefEPIt2nVfKfaPYEsKpa0mpVZLrS4Jd2PLnxNjj8euKP5XsWLr5n27RZTAAAAAAAAAAAAAAOfiWDUcS+8ht4STcZeq3+ZFkwUyfdCO+Ot/blrQq11s30kuTqvL4EccTHH4Q/6PH+d/5dawwihh33VGMX3ss5f1PaTVpWvqEtMNKfbDdO0rHc1421vKcnlGKbfgjm9orWZl5M6jaqscvJ4ne55ZyqSSjHlnlGKMWtpy5JtLJ5N5tOvlZGAYXHCMMjSXvb5vvTe9/TwRsY6dK6aWHFGOsVhq6YUOmwWT7koz8tz+DK/Or2xT/BmjdUBMNSAAAAB1tGMJt8SvZKrBuSjrR6zSeTyeaXijQ4PW8zW0OseDHe27QntraQs6OpThGEeyMUl8DXrWI9L1aVrGohnPXQAAAAAAAAAAAAAAAAAAAEU03v9WEaCe/rz8E9i9dvkZn6hl1EUhXz28acLQ6y9t0k137tGOt/ieyPzb8jng035VMFe+bf4hZCNVpsVzRVxbyg90ouL8Gsjm1e1ZiXkxuNKvuaDtbiVOXvRbT8uPgfN3pNLTWfwoTGp0xHLkAAAO9oXByxjPgqcs/NpF3gR/u7/hNg+5PDbXAAAAAAAAAAAAAAAAAAAAPj2ISKxxa79uxGdTg5bP0rYvgj5zPfvkmyhe27TKSf/PLbVwypVe+pVeX6Y7PnrGzxK6o64Vf2zb5lKy0ugEb0qwR3kemprrpdaPGUeXNGfzON3/fX2gy4+0bhCTHVQPABvD1PtFcKeH2blJZVJ5NruxW6PjtzNzh4Pp13PuVzFTrDuFxKAAAAAAAAAAAAAAAAAAABz8euPZcIqy46rS8ZdVfMg5N+mKZcXnVZVpN6sHyR8/WGfPpY2itD2fR2hH8tSfjLrfU+jxRqkQu8eusVYdUkTAADi4vo5TxGTkupN/iS2N81x8Spn4dMnmPEor4osjlfRS4py6urNcpZP0Zn24GWPXlBOC34eaOi1xUltjGK7XNP/TmeV4GWfZGG0pHg+jlPD5qcnr1FubWyPgvqaGDh0x+Z8ynpiivl3C4lAAAAAAAAAAAAAAAAAAAAAaWMWH2lh8qWeTeTT7JLas+RFnxfUpNXN69o0rnFcOq2NOSnTa2PalnF7ODRhzgvS0RMM/LS0VmJWZYQ6Oypx7IRXpFI+gr6hoU+2Gc9dAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//Z";
  render() {
    return (
      <ScrollView style={styles.container}>
        <Row style={styles.imagePosition}>
          <Image
            style={styles.imageSize}
            source={{
              uri: this.url
            }}
          />
        </Row>
        <Row style={styles.titlePosition}>
          <Text style={styles.titleStyle}>Aer Lingus (AE)</Text>
        </Row>
        <View style={styles.info}>
          <AirlineList
            miniLocation="Counter Location"
            floor="Second Floor"
            location="Central Terminal, Concourse F,"
            iconName="human-child"
            iconType="MaterialCommunityIcons"
          />

          <View>
            <AirlineList
              miniLocation="Arrivals"
              floor="First Floor"
              location="Central Terminal, Concourse F,"
              iconName="airplane-landing"
              iconType="MaterialCommunityIcons"
            />
            <AirlineList
              miniLocation="Departures"
              location="Concourse F, 2nd Level"
              iconName="airplane-takeoff"
              iconType="MaterialCommunityIcons"
            />
            <AirlineList
              miniLocation="Baggage Claim"
              location="Baggage Claim 15"
              iconName="shopping-bag"
              iconType="Foundation"
            />
            <AirlineList
              miniLocation="Website"
              location="www.aerlingus.com/"
              iconName="web"
              iconType="MaterialCommunityIcons"
            />
            <AirlineList
              miniLocation="Phone"
              location="+1 (516) 622-4226"
              iconName="phone"
              iconType="MaterialCommunityIcons"
            />
            <AirlineList
              miniLocation="Best Place to park"
              location="Flamingo Garage"
              iconName="mail-forward"
              iconType="FontAwesome"
            />
            <AirlineList
              miniLocation="Best Place to Pick Someone up"
              location="Arrivals Curbside"
              iconName="mail-forward"
              iconType="FontAwesome"
            />
            <AirlineList
              miniLocation="Best P;ace to Drop Someone Off"
              location="Departures Curbside"
              iconName="mail-forward"
              iconType="FontAwesome"
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imagePosition: {
    flex: 1.8,
    justifyContent: "center",
    alignItems: "center"
  },
  imageSize: {
    flex: 1,
    borderRadius: 50,
    aspectRatio: 3.7,
    resizeMode: "contain"
  },
  titlePosition: {
    flex: 0.4,
    paddingTop: 10,
    paddingLeft: 10
  },
  titleStyle: {
    flex: 1,
    fontSize: 15,
    color: "#164093"
  },
  info: {
    flex: 7,
    flexDirection: "column"
  }
});
