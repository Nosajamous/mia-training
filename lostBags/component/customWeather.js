import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Icon } from "native-base";

export default class CustomWeather extends Component {
  render() {
    return (
      <Grid style={styles.container}>
        <Row>
          <Text style={styles.content}> {this.props.dayName}</Text>
        </Row>
        <Row>
          <Icon
            style={styles.cloud}
            type="FontAwesome5"
            name={this.props.iconName}
          />
        </Row>
        <Row>
          <Text style={styles.degree}>81 </Text>
          <Text style={styles.degreeGrey}>65</Text>
        </Row>
      </Grid>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 65,
    padding: 1,
    marginLeft: 15
  },
  content: {
    fontSize: 18,
    marginTop: 3,
    color: "black"
  },
  cloud: {
    fontSize: 40,
    color: "#C1C1C1",
    paddingLeft: 5
  },
  degree: {
    fontSize: 15,
    color: "black",
    paddingRight: 5,
    paddingTop: 10
  },
  degreeGrey: {
    fontSize: 15,
    paddingLeft: 5,
    paddingTop: 10
  }
});
