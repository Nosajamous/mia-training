export default (InfoData = [
  {
    title: "Getting Around MIA",
    iconName: "retweet",
    iconType: "AntDesign"
  },
  {
    title: "Airport Map",
    iconName: "map-marker",
    iconType: "FontAwesome"
  },
  {
    title: "TSA Security Checkpoints",
    iconName: "mobile-phone",
    iconType: "FontAwesome"
  },
  {
    title: "International Arrivals",
    iconName: "ios-airplane",
    iconType: "Ionicons"
  },
  {
    title: "VIP Clubs & Lounges",
    iconName: "torso-business",
    iconType: "Foundation"
  },
  {
    title: "Art Exhibits",
    iconName: "artstation",
    iconType: "FontAwesome5Brands"
  },
  {
    title: "Accessible Travel",
    iconName: "wheelchair-alt",
    iconType: "FontAwesome"
  },
  {
    title: "Info Counter & Paging",
    iconName: "question-circle",
    iconType: "FontAwesome"
  }
]);
