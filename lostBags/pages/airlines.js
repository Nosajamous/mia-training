import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Button,
  Image,
  FlatList,
  Platform,
  TouchableOpacity,
  TouchableNativeFeedback
} from "react-native";
import AirlineInfo from "./resources/airlineInfo";
import { HeaderBackButton } from "react-navigation";
import { Icon } from "native-base";

export default class Airlines extends Component {
  constructor(props) {
    super(props);

    console.log(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <HeaderBackButton
          onPress={() => {
            navigation.navigate("Home");
          }}
        />
      ),
      title: "Welcome"
    };
  };

  FlatListItemSeparator = () => {
    return <View style={{ height: 2, width: "100%" }} />;
  };

  GetItem(item) {
    Alert.alert(item);
  }

  renderIOS(item) {
    return (
      <TouchableOpacity activeOpacity={0.1}>
        {this.renderListItem(item)}
      </TouchableOpacity>
    );
  }

  renderAndroid(item) {
    const { navigate } = this.props.navigation;
    return (
      <TouchableNativeFeedback
        activeOpacity={0.1}
        onPress={() => navigate("AirlineDetails")}
      >
        {this.renderListItem(item)}
      </TouchableNativeFeedback>
    );
  }

  renderListItem(item) {
    return (
      <View style={styles.position}>
        <Image
          style={styles.imageSize}
          source={{
            uri: item.imageName
          }}
        />
        <Text
          style={styles.item}
          // onPress={this.GetItem.bind(this, item.title)}
        >
          {item.title}
        </Text>
        <Icon
          name="chevron-right"
          type="EvilIcons"
          style={{
            fontSize: 30,
            color: "#BABDBD"
          }}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={AirlineInfo}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item }) =>
            Platform.OS == "ios"
              ? this.renderIOS(item)
              : this.renderAndroid(item)
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  position: {
    flex: 1,
    flexDirection: "row",
    height: 60,
    alignItems: "center"
  },
  item: {
    flex: 5,
    fontSize: 16,
    color: "black"
    // backgroundColor: "red"
  },
  imageSize: {
    flex: 1,
    borderRadius: 45,
    aspectRatio: 1.5,
    resizeMode: "contain"
    // backgroundColor: "blue"
  }
});
