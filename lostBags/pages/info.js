import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  FlatList,
  Platform,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableHighlight
} from "react-native";
import { Icon } from "native-base";
import InfoData from "./resources/infoData";

export default class Info extends Component {
  constructor(props) {
    super(props);
  }

  FlatListItemSeparator = () => {
    return <View style={{ height: 2, width: "100%" }} />;
  };

  GetItem(item) {
    Alert.alert(item);
  }

  renderIOS(item) {
    return (
      <TouchableOpacity activeOpacity={0.1}>
        {this.renderListItem(item)}
      </TouchableOpacity>
    );
  }

  renderAndroid(item) {
    return (
      <TouchableNativeFeedback activeOpacity={0.1}>
        {this.renderListItem(item)}
      </TouchableNativeFeedback>
    );
  }

  renderListItem(item) {
    return (
      <View style={styles.position}>
        <Icon style={styles.icon} name={item.iconName} type={item.iconType} />
        <Text
          style={styles.item}
          // onPress={this.GetItem.bind(this, item.title)}
        >
          {item.title}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={InfoData}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item }) =>
            Platform.OS == "ios"
              ? this.renderIOS(item)
              : this.renderAndroid(item)
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  position: {
    flex: 1,
    flexDirection: "row",
    // paddingTop: 10,
    height: 60
  },
  item: {
    flex: 4,
    paddingTop: 15,
    // backgroundColor: "red",
    fontSize: 16,
    // height: 50,
    color: "black"
  },
  icon: {
    flex: 1,
    textAlign: "center",
    // backgroundColor: "blue",
    paddingTop: 10,
    fontSize: 35,
    color: "#164093"
  }
});
